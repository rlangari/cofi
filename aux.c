#include <assert.h>
#include <stdio.h>

#include "aux.h"

int
elements_64b_equal(uint64_t *v, int vlen)
{
  for (int i=1; i < vlen; i++)
  {
    if (v[i] != v[0])
    {
      printf("run #%d: %lu vs. run #0: %lu\n", i, v[i], v[0]);
      return 0;
    }
  }
  return 1;
}

int
elements_equal(uint32_t *v, int vlen)
{
  for (int i=1; i < vlen; i++)
  {
    if (v[i] != v[0])
    {
      printf("run #%d: %u vs. run #0: %u\n", i, v[i], v[0]);
      return 0;
    }
  }
  return 1;
}

uint64_t
sum(uint64_t *v, int vlen)
{
  uint64_t vsum = 0;

  for (int i=0; i < vlen; i++)
     vsum += v[i];

  return vsum;
}

uint64_t
fast_pow2(int n)
{
  return (0x1LU << n);
}

double
get_alpha(int kstep, int dval)
{
    switch (kstep)
    {
        case 1:
            switch (dval)
            {
                case   1: return 1.346;
                case  32: return 1.089;
                case 192: return 1.069;
                default:  return 0.0;
            }

        case 2:
            switch (dval)
            {
                case   1: return 2.0;
                case  16: return 2.308;
                case  64: return 1.094; /* bv */
                case  96: return 1.085; /* bv */
                case 128: return 2.160;
                default:  return 0.0;
            }

        default:
            return 0;
    }
}

/*  version  p(%)  alpha
    k1d1     65.4  1.346
    k1d16    88.9  1.111
    k1d32    91.1  1.089
    k1d64    92.5  1.075
    k1d128   92.5  1.075
    k1d192   93.1  1.069
    k1d448   94.1  2.118
    k1d704   94.6  3.163
    k1d960   94.9  4.205
    k1d1984  95.6  8.352
    ---------------------
    version   p(%)  alpha
    k2d1       0.0  2.000
    k2d16     84.6  2.308
    k2d128    92.0  2.160
    k2d256    93.0  3.211
    k2d384    93.4  4.263
    k2d32bv   88.4  1.116
    k2d64bv   90.6  1.094
    k2d96bv   91.5  1.085
    k2d224bv  92.8  1.072
    k2d480bv  93.7  1.063
 */
