#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <divsufsort64.h>

#include "BWT.h"
#include "bit_mng.h"
//#include "divsufsort64.h"


/* Fast ceiling of an unsigned integer division */
uint64_t
ceil_uint_div(uint64_t x, uint64_t y)
{
    return (x/y + (x % y != 0));
}

int
get_unique_elements(char * in, char ** out, uint n)
{
  uint i;
  int j=0, size=20, count=0;

  *out = calloc(20, sizeof(char));
  // *out = (char*)malloc(20*sizeof(char));
  if (*out == NULL)
  {
    fprintf(stderr, "Error at output malloc for get_unique_elements \n");
    return -1;
  }

  for(i = 0; i < n; i++)
  {
    if (in[i] != '$')
    {
      for (j=0; j<count; j++)
      {
        if (in[i] == (*out)[j])
         break;
      }
      if (j == count)
      {
        if (count >= size)
        {
          *out = (char*) realloc(*out, (size+20)*sizeof(char));
          size += 20;
        }
        (*out)[count] = in[i];
        count++;
      }
    }
  }
  // ajuste de tamaño
  *out = (char*)realloc(*out, count*sizeof(char));
  return count;
}

/* in: input string
 * out: BWT
 * n: length of input string */
int
get_bwt(char** in, char*** out, uint64_t n, uint steps, uint64_t** end)
{
  saidx64_t * SA;

  // We add $ at the end of the string
  *in = realloc(*in, (n+2)*sizeof(char));
  (*in)[n] = '$'; (*in)[n+1] = 0;  //strcat(*in, "$");
  // before: n chars, n+1 allocated bytes
  // after: n+1 chars, n+2 allocated bytes

  // Memory allocation for the suffix array (SA)
  SA = (saidx64_t*) malloc((n+1)*sizeof(saidx64_t));
  if (SA == NULL)
  {
    fprintf(stderr, "Error at malloc for SA: %lu bytes (%.2f GB) requested but not allocated\n", (n+1)*sizeof(saidx64_t), (float) (n+1)*sizeof(saidx64_t)/1.0e9);
    return -1;
  }

  *end = (uint64_t*) malloc(steps*sizeof(uint64_t));
  if (*end == NULL)
  {
    fprintf(stderr, "Error at malloc for $ pos \n");
    return -2;
  }

  // suffix array calculation
  if (divsufsort64((sauchar_t*)(*in), SA, n+1) < 0)
  {
    fprintf(stderr, "Error when generating SA \n");
    return -3;
  }

  // Out memory allocation
  *out = (char**) malloc(steps*sizeof(char*));
  if (*out == NULL)
  {
    fprintf(stderr, "Error at malloc for BWT output \n");
    return -4;
  }
  for(uint64_t i = 0; i < steps; i++)
  {
    (*out)[i] = (char*) malloc(sizeof(char)*(n+1));
    if ((*out)[i] == NULL)
    {
      fprintf(stderr, "Error at malloc for BWT output \n");
      return -4;
    }
  }

  // BWT calculation from SA
  // for (uint64_t i=0; i<n+1; i++) {
  //    if (SA[i] > 0)      BWT[i] = T[SA[i]-1];
  //    else                BWT[i] = '$'; }
  for(uint64_t i=0; i < n+1; i++)
  {
    for(int64_t j=0; j < steps; j++)
    {
      if (SA[i] > j)
      {
        (*out)[j][i] = (*in)[SA[i]-1-j];
      }
      else if (SA[i] < j)
      {
        (*out)[j][i] = (*in)[n-j+SA[i]];
      }
      else
      {
        (*end)[j] = i;
        (*out)[j][i] = '$';  // (*out)[j][i] = (*in)[n];
      }
    }
  }

#if 0
  // una variante sobre la que trabajar
  for(uint64_t i=0; i<n+1; i++)
  {
    for(uint64_t j=0; j<steps; j++)
    {
      if (SA[i] != j)
      {
        (*out)[j][i] = (*in)[(SA[i]-j) > 0 ? SA[i]-1-j : n-j+SA[i]];
        // (*out)[j][i] = (*in)[(n - j + SA[i]) % (n + 1)];
      }
      else
      {
        (*end)[j] = i;
        (*out)[j][i] = '$';  //(*out)[j][i] = (*in)[n];
      }
    }
  }
#endif

  // 0 ended string
  for (int64_t i=0; i < steps; i++)
    (*out)[i][n+1] = 0;

  free(SA);
  return 0;
}

int
encode_bwt(char** bwt, char* codes, uint n_bwt, uint n_chars, uint steps, uint64_t* end)
{
  for(uint64_t k = 0; k < steps; k++)
  {
    for(uint64_t i = 0; i < n_bwt; i++)
    {
      for(uint64_t j = 0; j < n_chars; j++)
      {
        if (bwt[k][i] == codes[j])
        {
          bwt[k][i] = j;
          break;
        }
      }
    }
    // codificacion del $
    bwt[k][end[k]] = 0;
  }
  return 0;
}


int64_t
reduce_bwt(char** bwt, uint64_t n, uint n_bits, uint8_t*** buffer, uint steps)
{
  uint64_t i,j;
  uint64_t n_bytes = ceil_uint_div(n*(uint64_t)n_bits, 8);
  // uint64_t n_bytes = ceil(((double)n/8.0)*(double)n_bits);

  *buffer = (uint8_t**) malloc(steps*sizeof(uint8_t*));
  if (*buffer == NULL)
  {
    fprintf(stderr, "Error at malloc for BWT output \n");
    return -4;
  }
  for(i = 0; i<steps; i++)
  {
    (*buffer)[i] = calloc(n_bytes, sizeof(uint8_t));
    if ((*buffer)[i] == NULL)
    {
      fprintf(stderr, "Error at calloc for BWT buffer \n");
      return -4;
    }
  }

  for(j=0; j<steps; j++)
  {
    for(i=0; i<n; i++)
      write_char_to_buffer((*buffer)[j], n_bits, ((uint64_t)n_bits)*i, bwt[j][i]);
  }

  return n_bytes;
}


int
char_cmp_func(const void *a, const void *b)
{
  return *(char*)a - *(char*)b;
}
