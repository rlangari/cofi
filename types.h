#ifndef _TYPES_H_
#define _TYPES_H_

//#include <immintrin.h>
#include <stdint.h>

typedef unsigned int uint;

#ifdef __GNUC__
#define __forceinline inline
//#define __forceinline __attribute__((always_inline))
#define _popcnt64 __builtin_popcountll
#endif

struct block {
  uint64_t counter;
  //uint32_t padding;
  uint64_t bitmap;
};


struct FMIndex {
  uint64_t len;
  char * start;

  uint char_count; // number of unique symbols (4)
  uint char_bits;  // bits required to encode the symbols (2)
  char * characters;

  char last_char;
  uint64_t * end_char;
  uint64_t * C;

  uint64_t n_entries;
  uint char_per_entry;

  struct block * entries;

  // 1-char step encoding Table
  uint8_t * encoding_table;

  // 2-char step encoding table
  uint8_t * encoding_table2;

  uint *** LUT; // 5 and 6 characters LUTs

  uint * RLF;   // TODO
};

#endif
