#!/bin/bash
if [ "$#" -ne 1 ]; then
	echo "Illegal number of parameters"
	exit
fi
VERSION=k${1}inc
POW=$((4 ** ${1}))
cp -r k15inc ${VERSION}
cd ${VERSION}
rename k15inc ${VERSION} *
find . -type f -exec sed -i "s/k15inc/${VERSION}/g" {} +
sed -i "s/K2_SYMBOLS 1073741824/K2_SYMBOLS ${POW}/g" ${VERSION}.h
sed -i "s/KSTEPS 15/KSTEPS ${1}/g" ${VERSION}.h
rm -f cc_report/* obj/*

