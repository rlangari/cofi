#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <numa.h>
#include <numaif.h>
#include <sched.h> // For sched_setaffinity
#include <unistd.h>
  
#include "mem.h"

/* Adaptado de:
   https://github.com/ManuelSelva/c4fun/blob/master/mem_load/mem_load.c
 */
size_t
get_hugepage_size()
{
  const char *key = "Hugepagesize:";

  FILE *f = fopen("/proc/meminfo", "r");
  assert(f);

  char *linep = NULL;
  size_t n = 0;
  size_t size = 0;
  while (getline(&linep, &n, f) > 0)
  {
    if (strstr(linep, key) != linep) continue;
    size = atol(linep + strlen(key)) * 1024;
    break;
  }
  fclose(f);
  free(linep);
  assert(size);
  return size;
}

static void
print_oneline_file(char *filename)
{
  size_t len = 0;
  FILE * fp;
  char *linep = NULL;
  
  fp = fopen(filename, "r");
  if (fp == NULL)
  {
    printf("    NA");
    return;
  }

  if (-1 == getline(&linep, &len, fp))
	  printf("    NA");
  else
  {
      size_t linelen = strlen(linep);
      while ( (linep[linelen-1] == '\n') || (linep[linelen-1] == '\r') )
      {
        linep[linelen-1] = 0;
        linelen--;
      }
      printf("%6s", linep);
  }

  free(linep);
  fclose(fp);
}

void
hugepages_status()
{
  // Get numa configuration
  int numa_nodes;
  char string[512];

  // check if system supports NUMA API
  int available = numa_available();
  if (available == -1)
  {
      numa_nodes = 0;
      return;
  }
  else
      numa_nodes = numa_num_configured_nodes();

  printf("Free hugepages   2MB   1GB\n");
  for (int j=0; j < numa_nodes; j++)
  {
    printf("node %u:       ", j);
    sprintf(string, "/sys/devices/system/node/node%u/hugepages/hugepages-2048kB/free_hugepages", j);
    print_oneline_file(string);

    sprintf(string, "/sys/devices/system/node/node%u/hugepages/hugepages-1048576kB/free_hugepages", j);
    print_oneline_file(string);

    printf("\n");
  }
  // printf("\n\n");
}

int
mem_conf()
{
  // uint64_t nodes_mask = 0;
  int ncores = sysconf(_SC_NPROCESSORS_ONLN);
  int core = sched_getcpu();
  int node = 0, max_node = 0;
  int available = numa_available();

  // check if system supports NUMA API
  if (available == -1) return 0;

  max_node = numa_max_node();
  printf("System info: %d %s, %d cores (%d cores/node)\n",
          max_node+1, max_node == 0? "node":"nodes", ncores, ncores/(max_node+1));

  node = numa_node_of_cpu(core);
  printf("Process info: running in node %d, core %d.\n", node, core);

#if 0
  // node = 0;
  // to avoid the bootstrap processor (core 0)
  if (max_node == 1) node = 1;

  // restricts memory allocation to the node
  nodes_mask += 1UL << node;
  int status = set_mempolicy(MPOL_BIND, &nodes_mask, 8*sizeof(uint64_t));
  if (status == -1) perror("set_mempolicy");
#endif

#if 0
  status = numa_run_on_node(node); 
  if (status == -1) perror("numa_run_on_node");
  else printf("Pinned to node %d.\n\n", node);
#endif

#if 0
  /* Pin process on core CPU */
  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(core, &mask);
  if (sched_setaffinity(0, sizeof(mask), &mask) == -1)
    perror("sched_setaffinity");
#endif

  return node;
}
