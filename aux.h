#ifndef _AUX_H_
#define _AUX_H_

#include <stdint.h>

// Gi = Gibi = 2^30
#define KiB  (1024UL)
#define MiB  (KiB*1024UL)
#define GiB  (MiB*1024UL)

#define KILO  (1000.0)
#define MEGA  (1000000.0)
#define GIGA  (1000000000.0)

#define HLINE "-------------------------------------------------------------\n"

int elements_64b_equal(uint64_t *v, int vlen);

int elements_equal(uint32_t *v, int vlen);

uint64_t sum(uint64_t *v, int vlen);

uint64_t fast_pow2(int n);

double get_alpha(int kstep, int dval);

#endif
