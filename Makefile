# Makefile written by Jesus Alastruey Benede (jalastru@unizar.es)
# October 2017

# Source file
VERSION=$(shell basename $(CURDIR))

# Select the compiler,
#    c=0 corresponds icc
#    c=1 corresponds gcc-6
#    c=2 corresponds gcc-7
# You can override the default value from the command line!
# The command
#    "make c=1"
# will the use value c=1, regardless of the default.
CC=icc
c=0

ifeq ($(c),0)
    CC=icc
else ifeq ($(c),1)
    CC=gcc
else ifeq ($(c),2)
    CC=gcc-7
endif

# Specify number of threads (t)
# The command
#    "make t=1"
# will the use value t=1
t=0
ifeq ($(t),0)
    THREAD_FLAG =
else ifeq ($(t),1)
    THREAD_FLAG = -DTHREADS=$(t) -g
else
    THREAD_FLAG = -DTHREADS=$(t)
endif

# Specify number of overlapped sequence searches (s)
# The command
#    "make s=4"
# will the use value s=4
s=4
OVERLAP_FLAG = -DNSEQS=$(s)

# Select the target architecture,
#    a=0 corresponds to native architecture (native)
#    a=1 corresponds to Knights Landing (knl)
#    a=2 corresponds to Ivy Bridge architecture (ivb)
#    a=3 corresponds to Haswell architecture (hsw)
#    a=4 corresponds to Broadwell architecture (bdw)
#    a=5 corresponds to Skylake client architecture (skl)
#    a=6 corresponds to Skylake server architecture (skx)
# You can override the default value from the command line!
# The command
#    "make a=1"
# will the use value a=1, regardless of the default.
a=0

ifeq ($(a),0)
    ARCH=nat
else ifeq ($(a),1)
    ARCH=knl
else ifeq ($(a),2)
    ARCH=ivb
else ifeq ($(a),3)
    ARCH=hsw
else ifeq ($(a),4)
    ARCH=bdw
else ifeq ($(a),5)
    ARCH=skl
else ifeq ($(a),6)
    ARCH=skx
else
    ARCH=nat
endif

# Compiler dependent flags
# machine flags
ifeq ($(c),0)
    # icc
    ifeq ($(a),0)
        ARCH_FLAG= -xHost
    else ifeq ($(a),1)
        ARCH_FLAG = -xMIC-AVX512 -DKNL
    else ifeq ($(a),2)
        ARCH_FLAG = -xAVX
    else ifeq ($(a),3)
        ARCH_FLAG = -xCORE-AVX2
    else ifeq ($(a),4)
        ARCH_FLAG = -xCORE-AVX2
    else ifeq ($(a),5)
        ARCH_FLAG = -march=skylake
        # ARCH_FLAG = -xCORE-AVX2
    else ifeq ($(a),6)
        ARCH_FLAG = -xCORE-AVX512
    else
        ARCH_FLAG= -xHost
    endif
else
    # gcc
    ifeq ($(a),0)
        ARCH_FLAG = -march=native
    else ifeq ($(a),1)
        ARCH_FLAG = -march=knl  -DKNL
    else ifeq ($(a),2)
        ARCH_FLAG = -march=ivybridge
    else ifeq ($(a),3)
        ARCH_FLAG = -march=haswell
    else ifeq ($(a),4)
        ARCH_FLAG = -march=broadwell
    else ifeq ($(a),5)
        ARCH_FLAG = -march=skylake
    else ifeq ($(a),6)
        ARCH_FLAG = -march=skylake-avx512
    else
        ARCH_FLAG = -march=native
    endif
endif
# knl:            -mavx512f -mavx512cd -mavx512er -mavx512pf
# skylake-avx512: -mavx512f -mavx512cd -mavx512bw -mavx512dq -mavx512vl -mavx512ifma -mavx512vbmi

# More compiler dependent flags
# optimization and report flags
# -std=gnu11 is the default mode, but it is needed by the scan-build clang static analyzer
ifeq ($(c),0)
    # icc
    CFLAGS = -O3 -W -Wall -Winline -ip -qopenmp $(ARCH_FLAG) -std=gnu11  -qno-opt-prefetch 
    # CFLAGS = -O0 -g  ...
    REPORT_FLAGS = -qopt-report=4 -qopt-report-phase ipo -qopt-report-file=stdout
else
    # gcc
    CFLAGS = -O3 -W -Wall -Wextra -Wshadow -Winline -fopenmp $(ARCH_FLAG) -std=gnu11 -fno-prefetch-loop-arrays
    # CFLAGS = -O0 -g -W -Wall -Wextra -Wshadow -Winline -fopenmp $(ARCH_FLAG) -std=gnu11 -fno-prefetch-loop-arrays
    REPORT_FLAGS = -fopt-info-optimized
endif

# iaca support
i=0
ifeq ($(i),1)
    CFLAGS := $(CFLAGS) -DIACA
endif

# perf counters
w=0
ifeq ($(w),1)
    CFLAGS := $(CFLAGS) -DPERF
endif

# huge page support
CFLAGS := $(CFLAGS) -DHUGEPAGES

CLIBS = -lm -lmemkind -ldivsufsort64 -lnuma -L../lib -I../include

#
# Directories
#
VPATH = ..
OBJDIR := obj
BINDIR := ../bin
REPDIR := cc_report

#
# all sources
#
SRCS = $(wildcard *.c)

#
# common sources and objects
#
COMMON_SRCS = aux.c mem.c file_mng.c BWT.c bit_mng.c perf.c $(VERSION).c
COMMON_OBJS = $(patsubst %.c, $(OBJDIR)/%.o, $(COMMON_SRCS))

#
# all target
#
all: $(BINDIR)/$(VERSION)_build.$(ARCH).$(CC)  $(BINDIR)/$(VERSION)_fcount.$(ARCH).$(CC).$(t)th.$(s)seq

#
# alias to avoid errors when executing, for instance, $ make k15inc_fcount
#
$(VERSION)_build.$(ARCH).$(CC): $(BINDIR)/$(VERSION)_build.$(ARCH).$(CC)
	@echo "HOLA" > /dev/null

$(VERSION)_fcount.$(ARCH).$(CC).$(t)th.$(s)seq: $(BINDIR)/$(VERSION)_fcount.$(ARCH).$(CC).$(t)th.$(s)seq
	@echo "HOLA" > /dev/null

#
# dependencies to force the creation of the $(OBJDIR) and $(BINDIR) directories
# @: suppress the echoing of the command
#
$(OBJDIR):
	@mkdir -p $@
$(BINDIR):
	@mkdir -p $@
$(REPDIR):
	@mkdir -p $@	

#
# there are three different kinds of compilation lines
# https://www.gnu.org/software/make/manual/html_node/Static-Usage.html
#
# $<: name of the first prerequisite
# $@: file name of the target of the rule
# $(*F): the file-within-directory part of the stem. If the value of ‘$@’ is dir/foo.o then ‘$(*F)’ is foo 
# http://www.gnu.org/software/make/manual/make.html#Automatic-Variables
#
SRCS1 = aux.c mem.c file_mng.c BWT.c bit_mng.c perf.c $(VERSION).c $(VERSION)_build.c $(VERSION)_count.c

$(patsubst %.c, $(OBJDIR)/%.o, $(SRCS1)): $(OBJDIR)/%.o: %.c | $(OBJDIR) $(REPDIR)
	@$(CC)  $(CFLAGS)  -c $<  $(CLIBS)  -o $@  > $(REPDIR)/$(VERSION).$(*F).$(ARCH).$(CC).txt 2>&1

SRCS2 = $(VERSION)_fcount.c 
$(patsubst %.c, $(OBJDIR)/%.o, $(SRCS2)): $(OBJDIR)/%.o: %.c | $(OBJDIR) $(REPDIR)
	@$(CC)  $(CFLAGS) $(THREAD_FLAG) $(OVERLAP_FLAG) $(REPORT_FLAGS) -g -c $<  $(CLIBS) -o $@  > $(REPDIR)/$(VERSION)_fcount.$(ARCH).$(CC).$(t)th.$(s)seq.report.txt 2>&1
#	$(CC)  $(CFLAGS) $(THREAD_FLAG) $(OVERLAP_FLAG) $(REPORT_FLAGS) -g -c $<  $(CLIBS) -o $@  -Wa,-adghln=$(VERSION)_fcount.$(ARCH).$(CC).s   > $(VERSION)_fcount.$(ARCH).$(CC).report.txt 2>&1

#
# add this line to generate assembly code
#	-Wa,-adghln=$(VERSION)_fcount.$(ARCH).$(CC).s
#

# create list of auto dependencies
AUTODEPS:= $(patsubst %.c, $(OBJDIR)/%.d, $(SRCS))
# https://www.gnu.org/software/make/manual/html_node/Include.html#Include
-include $(AUTODEPS)

#
# binaries
# $^: names of all the prerequisites
# http://www.gnu.org/software/make/manual/make.html#Automatic-Variables

$(BINDIR)/$(VERSION)_build.$(ARCH).$(CC): $(COMMON_OBJS) $(OBJDIR)/$(VERSION)_build.o | $(BINDIR)
	$(CC)  $(CFLAGS)  $^  $(CLIBS)  -o $@  && strip $@

$(BINDIR)/$(VERSION)_fcount.$(ARCH).$(CC).$(t)th.$(s)seq: $(COMMON_OBJS) $(OBJDIR)/$(VERSION)_fcount.o | $(BINDIR)
	$(CC)  $(CFLAGS)  $^  $(CLIBS)  -o $@  && strip $@

#
# other targets
#
clean:
	@rm -f $(OBJDIR)/*.o $(OBJDIR)/*.d $(BINDIR)/$(VERSION)_build.$(ARCH).$(CC) $(BINDIR)/$(VERSION)_count.$(ARCH).$(CC) $(VERSION)_fcount.$(ARCH).$(CC).$(t)th.$(s)seq

flags:
	@$(CC) $(CFLAGS) -E -v - </dev/null 2>&1 | grep cc1

gccversion:
	@$(CC) -v 2>&1 | tail -1

.PHONY: clean all flags gccversion
