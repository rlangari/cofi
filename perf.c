#define _GNU_SOURCE 1

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>
#include <time.h>
#include <errno.h>
#include "perf.h"

/* static variables */

static char *events[HW_CTRS] = {"Instructions", "Cache references", "Cache misses", "LLC loads"};
static char *short_events[HW_CTRS] = {"Ir", "Cr", "Cm", "Cl"};  // notacion valgrind
static int fd[HW_CTRS];
static uint64_t hw_counters[MAXRUNS][HW_CTRS] = {{0}};

/* forward declarations */
void perf_reset();


/* ************************************************************************** */
/* static functions */
/* ************************************************************************** */

// Open a perf event
static int
perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags)
{
    return syscall(__NR_perf_event_open, hw_event, pid, cpu, group_fd, flags);
}

// Generate the perf struct
static void
perfStruct(struct perf_event_attr *pe, int type, int config)
{
    memset(pe, 0, sizeof(struct perf_event_attr));
    pe->size = sizeof(struct perf_event_attr);
    pe->disabled = 1;

    // Exclude kernel and hipervisor from being measured
    pe->exclude_kernel = 1;
    pe->exclude_hv = 1;

    /* children inherit it */
    pe->inherit=1;

    // Type of event to measure
    pe->type = type;
    pe->config = config;
}

/* para facilitar parseo */
static void
perf_print_one_line(uint64_t *count)
{
    // print short events
    printf("\n");
    for (int i = 0; i < HW_CTRS; i++) printf("  %14s", short_events[i]);
    printf("  %8s\n", "Bcmpki");

    // print counters
    for (int i = 0; i < HW_CTRS; i++) printf("  %14lu", count[i]);

    /* Branch conditional prediction misses per kiloinstruction */
    printf("  %8.2f", (1000.0*count[2])/count[0]);
    printf("    hw_counters\n");
}

/* imprime valor de contadores almacenados en variable pasada como parametro*/
static void
perf_print_counters(uint64_t *count, char *title)
{
    printf("%s\n", title);
    for (int i = 0; i < HW_CTRS; i++)
        printf("  %16s: %10.2fM\n", events[i], count[i]/1e6);

    /* Branch conditional prediction misses per kiloinstruction */
    printf("  %16s: %10.2f\n", "Bcmpki", (1000.0*count[2])/count[0]);

    /* para facilitar parseo */
    perf_print_one_line(count);
}


/* ************************************************************************** */
/* public functions */
/* ************************************************************************** */

/* inicializa contadores hardware */
int
perf_init()
{
    struct perf_event_attr pe[HW_CTRS];
    // Type of events
    //int type[HW_CTRS] = {PERF_TYPE_HARDWARE, PERF_TYPE_HARDWARE, PERF_TYPE_HARDWARE, PERF_TYPE_HW_CACHE};
    int type[HW_CTRS] = {PERF_TYPE_HARDWARE, PERF_TYPE_HARDWARE, PERF_TYPE_HARDWARE, PERF_TYPE_HARDWARE};
    // Name of the events to measure
    int event[HW_CTRS] = {PERF_COUNT_HW_INSTRUCTIONS, PERF_COUNT_HW_CACHE_REFERENCES, PERF_COUNT_HW_CACHE_MISSES, PERF_COUNT_HW_CACHE_MISSES};

    // Get perfStruct
    for (int i = 0; i < HW_CTRS; ++i) perfStruct(&pe[i], type[i], event[i]);
    // Open perf leader
    for (int i = 0; i < HW_CTRS; ++i)
    {
        if ((fd[i] = perf_event_open(&pe[i], 0, -1, -1, 0)) == -1)
        {
            fprintf(stderr,"Error opening event %llx %s\n", pe[i].config, strerror(errno));
            return -1;
        }
    }
    perf_reset();
    return 0;
}

/* resetea contadores hardware */
void
perf_reset()
{
    // Enable descriptor to read hw counters
    for (int i = 0; i < HW_CTRS; ++i)
    {
        ioctl(fd[i], PERF_EVENT_IOC_RESET, 0);
    }
}

/* activa contadores hardware */
void
perf_enable()
{
    // Enable descriptor to read hw counters
    for (int i = 0; i < HW_CTRS; ++i)
    {
        ioctl(fd[i], PERF_EVENT_IOC_ENABLE, 0);
    }
}

/* inicializa, resetea y activa contadores hardware */
int
perf_start()
{
    int n;

    n = perf_init();
    if (n == -1) return -1;
    perf_reset();
    perf_enable();
    return 0;
}

/* desactiva contadores hardware (dejan de contar los eventos) */
void
perf_stop()
{
    // "Turn off" hw counters
    for (int i = 0; i < HW_CTRS; ++i) ioctl(fd[i], PERF_EVENT_IOC_DISABLE, 0);
}

/* lee los contadores hw */
int
perf_read_sample(uint32_t run)
{
    // Reading HW counters
    for (int i = 0; i < HW_CTRS; i++)
    {
        if (sizeof(uint64_t) != read(fd[i], &hw_counters[run][i], sizeof(uint64_t)))
        {
            printf("\nERROR: lectura incorrecta en contador %16s\n", events[i]);
            return -1;
        }
    }
    return 0;
}

/* cierra los descriptores asociados a los contadores */
void
perf_close()
{
    // Close file descriptors
    for (int i = 0; i < HW_CTRS; ++i) close(fd[i]);
}

/* imprime valor de contadores */
void
perf_print()
{
    // Reading HW counters
    perf_read_sample(0);

    // Close file descriptors
    perf_close();

    // print counters
    perf_print_counters(hw_counters[0], "Hardware counters");
}

void
perf_print_samples(int nruns)
{
    uint64_t count_avg[HW_CTRS] = { 0 };
 
    printf("Hardware counters samples\n");
    printf("      \t");
    for (int j = 0; j < HW_CTRS; j++)
        printf("%14s\t", short_events[j]);
    printf("\n");

    // Print samples and compute average
    for (int i = 0; i < nruns; i++)
    {
        printf("run #%d\t", i);
        for (int j = 0; j < HW_CTRS; j++)
        {
            if (i != 0) count_avg[j] += hw_counters[i][j];
            printf("%14lu\t", hw_counters[i][j]);
        }
        printf("\n");
    }
    printf("avg.  \t");
    for (int j = 0; j < HW_CTRS; j++)
    {
        count_avg[j] /= (nruns-1);
        printf("%14lu\t", count_avg[j]);
    }
    printf("\n\n");

    perf_print_counters(count_avg, "Hardware counters average");
}
