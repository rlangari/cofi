# COFI: *CO*mpressed *F*M-*I*ndex for large K-steps

## Introduction

COFI is a tool for sequence alignment.
Specifically, it implements an exact search algorithm, using the FM-Index data structure, that counts the number of matches of arbitrary length reads on a reference genome.
COFI enables a 15-step FM-index using less than 16 GB of memory for a human genome reference of 3 giga base pairs.
We have evaluated an algorithm based on this new layout on both a Knights Landing (KNL) and an Skylake-based system (SKX). On these machines, COFI achieves average speed-ups of 1.46X and 1.39X, respectively, with respect to an state-of-the-art FM-index implementation that is already well optimized.

COFI is distributed under the GPLv3 license, and it runs on the command line under Linux.

## Requirements

- divsufsort64
- memkind (hbwmalloc)
- libnuma

In case you want to install the libraries locally, you can do it in the directories *include* and *lib*, the compilation process will search in these directories.

## Usage

We provide a small input reference (GRCh38\_1MB) and a file with 1 million sequences (test.fasta).
First, to generate the index go to the `scripts` directory an execute `./build_all.sh`.
Then, to execute COFI over the provided test inputs run `./run_all.sh`.
The final results can be found in the `runs` folder.
Note that the provided input set is not representative of a real workload of genomics.

We can change the values of `./run_all.sh` in order to tune the number of threads, interleaved sequences or inputs files.

## Performance

We have tested COFI against the split bit-vector sampled FM-index proposal (<https://github.com/chusAB/bvSFMindex>).
We have obtained the following speed-up with respect to this baseline for different inputs:

<p align="center">
<img src="images/speedup_knl_38.png" height="400"/a></br>
</p>

For more information read the article.

## Citation

If you use COFI for your published research, please cite the following paper:

R. Langarita, A. Armejach, J. Setoain, P. E. Ibanez Marin, J. Alastruey-Benede and M. Moreto Planas, "Compressed Sparse FM-Index: Fast Sequence Alignment Using Large K-Steps," in IEEE/ACM Transactions on Computational Biology and Bioinformatics, doi: 10.1109/TCBB.2020.3000253.

