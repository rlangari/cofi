#!/bin/bash

# default values
version="k15inc"
arch=nat
comp=gcc
nthreads=8
nseqs=16
nruns=2

perf=0
outdir=runs

gref=GRCh38_1MB
file_seq=test.fasta

while getopts "a:v:c:t:s:p:w:g:f:n:e:h" opt; do
  case $opt in
    a) 
      # echo "architecture -> $OPTARG"
      arch=$OPTARG
      ;;
    v) 
      # echo "version -> $OPTARG"
      version=$OPTARG
      ;;
    c) 
      # echo "compiler -> $OPTARG"
      comp=$OPTARG
      ;;
    t) 
      # echo "number of threads -> $OPTARG"
      nthreads=$OPTARG
      ;;
    s) 
      # echo "number of overlapped sequences -> $OPTARG"
      nseqs=$OPTARG
      ;;
    g) 
      # echo "reference genome -> $OPTARG"
      gref=$OPTARG
      ;;
    f) 
      # echo "file with sequences to search -> $OPTARG"
      file_seq=$OPTARG
      ;;
    w)
      # echo "collect hardware counters with perf -> $OPTARG"
      perf=$OPTARG
      ;;
    n)
      nruns=$OPTARG
      ;;
    h)
      echo "uso:"
      echo "$0 -v version  -c compiler -a architecture -g reference_genome -f sequence"
      echo "example:"
      echo "$0 -v k15inc -c gcc-7 -a knl -g GRCh38_1MB -f test.fasta"
      exit
      ;;
    \?)
      echo "invalid option: -$OPTARG"
      ;;
    :)
      echo "-$OPTARG option requires a parameter"
      exit 1
      ;;
  esac
done

if [ ${perf} -eq 1 ]; then
    outdir=perf
fi

mkdir -p ../${outdir}
fseq=${file_seq%.*}
nth=`printf "%02d\n" $nthreads`
nsq=`printf "%02d\n" $nseqs`

outfile=../${outdir}/${version}.${arch}.${comp}.${nth}th.${nsq}seq.${gref}.${fseq}.`date +%Y%m%d.%02H%M%S`.`hostname | cut -f1 -d.`.txt
touch ${outfile}
echo -n "Test machine: "  >> ${outfile}
hostname >> ${outfile}
echo -n "Model name:"  >> ${outfile}
cat /proc/cpuinfo | grep 'model name' | uniq | cut -f2 --delimiter=: >> ${outfile}
echo -n "Date: " >> ${outfile} 
LANG=en_EN date >> ${outfile}

PREFIX=../bin
bin=${PREFIX}/${version}_fcount.${arch}.${comp}.${nthreads}th.${nseqs}seq

reffile=../References/${gref}.${version}.fmi
if [ ! -f "$reffile" ]
then
	echo "Reference not found"
	exit
fi

file_seq_aux="../Sequences/${file_seq}"
if [ ! -f "$file_seq_aux" ]
then
	echo "Sequences not found"
	exit
fi

echo -n "executing ${bin} ${reffile} ${file_seq_aux} ... "
${bin} ${reffile} ${file_seq_aux} ${nruns} >> ${outfile} 2>&1

echo >> ${outfile}
printf "OK\n\n"
