#!/bin/bash

# uso:
#    ./compile_all.sh

version_list=("k15inc")

arch_list=("nat")

comp_list=("gcc")
# comp_list=("icc" "gcc-6" "gcc-7" )

# number of hardware threads
nthreads=28

# number of overlapped searches
nseq_list=("16")
# nseq_list=("4" "8" "16" "24" "32" "40")
# nseq_list=("4" "6" "8" "10" "12" "14" "16")

# collect hw counters with perf
perf=0

# loop over the versions
for version in "${version_list[@]}"
do
    # loop over the architectures
    for arch in "${arch_list[@]}"
    do
        # loop over the compilers
        for comp in "${comp_list[@]}"
        do
            # loop over the overlapped searches
            for nseq in "${nseq_list[@]}"
            do
                # -b: compile FMindex build program
                # -d: compile FMindex count program
                ./compile.sh -v $version -a $arch -c $comp -t $nthreads -s $nseq -w $perf
            done
        done
    done
done
