#!/bin/bash

# use:
#    ./build_all.sh

version_list=("k15inc")
arch_list=("nat")
# arch_list=("knl" "bdw" "skx" "ivb" "native")

comp_list=("gcc" )
# comp_list=("icc" "gcc-6" "gcc-7" )

# reference genome
gref=GRCh38_1MB

# loop over the versions
for version in "${version_list[@]}"
do
    # loop over the architectures
    for arch in "${arch_list[@]}"
    do
        # loop over the compilers
        for comp in "${comp_list[@]}"
        do
            ./compile.sh -v $version -a $arch -c $comp -b
            ./build_fmindex.sh -v $version -a $arch -c $comp -g $gref
            # echo -v $version -a $arch -c $comp
        done
    done
done
