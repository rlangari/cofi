#!/bin/bash

version_list=("k15inc")

arch_list=("nat")
#arch_list=("nat" "skx" "knl")

comp_list=("gcc")
#comp_list=("gcc" "icc")

# number of hardware threads
nthreads_list=("8")

# number of overlapped searches
nseq_list=("8" "16")

nruns=2

# genoma de referencia
gref=GRCh38_1MB

# fichero con las secuencias de bases a buscar
seq_list=("test.fasta")

# collect hw counters with perf
perf=0

# loop over the versions
for version in "${version_list[@]}"
do
    # loop over the architectures
    for arch in "${arch_list[@]}"
    do
        # loop over the compilers
        for comp in "${comp_list[@]}"
        do
            # loop over the overlapped searches
            for nthreads in "${nthreads_list[@]}"
            do
                # loop over the overlapped searches
                for nseq in "${nseq_list[@]}"
                do
                    ./compile.sh -v $version -a $arch -c $comp -t $nthreads -s $nseq -w $perf
                    for file_seq in "${seq_list[@]}"
                    do
                        # -b: ejecucion de build, -d: ejecucion de count
                        ./run.sh -v $version -a $arch -c $comp -t $nthreads -s $nseq -w $perf -g $gref -f $file_seq -n $nruns
                    done
                done
            done
        done
    done
done
