#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#ifdef KNL
#include <hbwmalloc.h>
#endif

#include "mem.h"
#include "file_mng.h"

int
file_to_char(const char * file, char ** data)
{
  FILE *f = fopen(file, "rb");
  if (f == NULL)
  {
    fprintf(stderr, "Cannot open file %s \n", file);
    return -1;
  }

  fseek(f, 0, SEEK_END);
  long fsize = ftell(f);
  fseek(f, 0, SEEK_SET);

  *data = malloc(fsize + 1);
  uint r = fread(*data, sizeof(char), fsize/sizeof(char), f);
  if (r != (fsize/sizeof(char)))
  {
    fprintf(stderr, "Cannot read data from file %s \n", file);
    return -2;
  }
  fclose(f);

  if((*data)[fsize-1] != '\n' && (*data)[fsize-1] != '\r')
    (*data)[fsize] = 0;
  else while((*data)[fsize-1] == '\n' || (*data)[fsize-1] == '\r')
  {
    (*data)[fsize-1] = 0;
    fsize--;
  }

  return 0;
}


int write_fmindex(const char* file, struct FMIndex * fmi)
{
  FILE *f = fopen(file, "w");
  if (f == NULL) {
    fprintf(stderr, "Cannot open file %s \n", file);
    return -1;
  }
  uint n_counters = pow(fmi->char_count, 2 /* nsteps */);

  // Write prologue
  fwrite(fmi->start, sizeof(char), 500, f);
  fwrite(&fmi->len, sizeof(fmi->len), 1, f);
  fwrite(&fmi->char_count, sizeof(fmi->char_count), 1, f);
  fwrite(&fmi->char_bits, sizeof(fmi->char_bits), 1, f);
  fwrite(fmi->characters, sizeof(char), fmi->char_count, f);
  fwrite(fmi->end_char, sizeof(uint64_t), 2, f);
  // Write C array
  fwrite(fmi->C, sizeof(uint64_t), n_counters+1, f);
  // Write Occ entries
  fwrite(&fmi->n_entries, sizeof(fmi->n_entries), 1, f);
  fwrite(&fmi->char_per_entry, sizeof(fmi->char_per_entry), 1, f);
  fwrite(fmi->entries, sizeof(struct block),
        (size_t)(fmi->n_entries*n_counters), f);

  // Write encoding table
  fwrite(fmi->encoding_table, sizeof(uint8_t), 256, f);
  fwrite(fmi->encoding_table2, sizeof(uint8_t), 256*256, f);

  // Write LUTs
  for(uint32_t i=0; i < pow(fmi->char_count, 5); i++) //5-chars LUT
    fwrite(fmi->LUT[0][i], sizeof(uint), 2, f);

  for(uint32_t i=0; i < pow(fmi->char_count, 6); i++) //6-chars LUT
    fwrite(fmi->LUT[1][i], sizeof(uint), 2, f);

  fclose(f);
  return 0;
}


int load_fmindex(const char* file, struct FMIndex * fmi)
{
  FILE *f = fopen(file, "rb");
  if (f == NULL) {
    fprintf(stderr, "Cannot open file %s\n", file);
    exit(EXIT_FAILURE);
  }
  long fr;

  fmi->start = malloc(sizeof(char)*501);
  fr = fread(fmi->start, sizeof(char), 500, f);
  if (fr != 500)
  {
    fprintf(stderr, "Error at fread(start)\n");
    exit(EXIT_FAILURE);
  }
  fmi->start[500]=0;

  //Read prologue
  fr = fread(&fmi->len, sizeof(fmi->len), 1, f);
  if(fr != 1)
  {
    fprintf(stderr, "Error at fread(len)\n");
    exit(EXIT_FAILURE);
  }

  fr = fread(&fmi->char_count, sizeof(uint), 1, f);
  if(fr != 1)
  {
    fprintf(stderr, "Error at fread(char_count)\n");
    exit(EXIT_FAILURE);
  }

  fr = fread(&fmi->char_bits, sizeof(uint), 1, f);
  if(fr != 1)
  {
    fprintf(stderr, "Error at fread(char_bits)\n");
    exit(EXIT_FAILURE);
  }

  uint n_counters = pow(fmi->char_count, 2);
  fmi->C = calloc(n_counters, sizeof(uint64_t));
  fmi->characters = malloc(fmi->char_count*sizeof(char));
  if ((fmi->C == NULL) || (fmi->characters == NULL))
  {
    fprintf(stderr, "Error when malloc fm-index memory\n");
    exit(EXIT_FAILURE);
  }
  fr = fread(fmi->characters, sizeof(char), fmi->char_count, f);
  if (fr != fmi->char_count)
  {
    fprintf(stderr, "Error at fread(characters)\n");
    exit(EXIT_FAILURE);
  }
  fmi->end_char = malloc(sizeof(uint64_t)*2);
  fr = fread(fmi->end_char, sizeof(uint64_t), 2, f);
  if (fr != 2)
  {
    fprintf(stderr, "Error at fread(end_char)\n");
    exit(EXIT_FAILURE);
  }

  // read C array
  fr = fread(fmi->C, sizeof(uint64_t), n_counters+1, f);
  if (fr != n_counters+1)
  {
    fprintf(stderr, "Error at fread(C)\n");
    exit(EXIT_FAILURE);
  }

  // Read Occ entries
  fr = fread(&fmi->n_entries, sizeof(fmi->n_entries), 1, f);
  if(fr != 1)
  {
    fprintf(stderr, "Error at fread(n_entries)\n");
    exit(EXIT_FAILURE);
  }
  fr = fread(&fmi->char_per_entry, sizeof(fmi->char_per_entry), 1, f);
  if (fr != 1)
  {
    fprintf(stderr, "Error at fread(char_per_entry)\n");
    exit(EXIT_FAILURE);
  }

  // Rocc entries memory allocation and reading
  #ifdef KNL
    #ifdef MEM_DDR4
      fmi->entries = aligned_alloc(64 ,
        ((long)fmi->n_entries*(long)n_counters)*(long)sizeof(struct block));
      if (fmi->entries == NULL)
      {
        fprintf(stderr, "%li Error at aligned_alloc (Entries)\n", fr);
        exit(EXIT_FAILURE);
      }
    #else
      fr = hbw_posix_memalign_psize((void**)&fmi->entries, 64,
        ((long)fmi->n_entries*(long)n_counters)*(long)sizeof(struct block),
        HBW_PAGESIZE_1GB);
      if (fr != 0)
      {
        fprintf(stderr, "%li Error at hbwmalloc (Entries)\n", fr);
        exit(EXIT_FAILURE);
      }
    #endif
  #else
    fr = posix_memalign((void**)&fmi->entries, ALIGN_2MB,
      ((long)fmi->n_entries*(long)n_counters)*(long)sizeof(struct block));
    if (fr != 0)
    {
      fprintf(stderr, "%li Error at malloc (Entries)\n", fr);
      exit(EXIT_FAILURE);
    }
  #endif

  fr = fread(fmi->entries, sizeof(struct block),
    ((long)fmi->n_entries*(long)n_counters), f);
  if (fr != ((long)fmi->n_entries*(long)n_counters))
  {
    fprintf(stderr, "Error at fread (Entries)\n");
    exit(EXIT_FAILURE);
  }

  // Read encoding table
  fmi->encoding_table = (uint8_t*)malloc(sizeof(uint8_t)*256);
  fr = fread(fmi->encoding_table, sizeof(uint8_t), 256, f);
  if (fr != 256)
  {
    fprintf(stderr, "Error at fread (Encoding table)\n");
    exit(EXIT_FAILURE);
  }

  fmi->encoding_table2 = (uint8_t*)malloc(sizeof(uint8_t)*256*256);
  fr = fread(fmi->encoding_table2, sizeof(uint8_t), 256*256, f);
  if (fr != 256*256)
  {
    fprintf(stderr, "Error at fread (Encoding table)\n");
    exit(EXIT_FAILURE);
  }

  // read LUTs
  uint lut_entries5 = pow(fmi->char_count, 5);
  uint lut_entries6 = pow(fmi->char_count, 6);
  fmi->LUT =  (uint***) malloc(sizeof(uint**)*2);
  fmi->LUT[0] =  (uint**) malloc(sizeof(uint*)*lut_entries5);
  fmi->LUT[1] =  (uint**) malloc(sizeof(uint*)*lut_entries6);
  if ((fmi->LUT == NULL) || (fmi->LUT[0] == NULL) || (fmi->LUT[1] == NULL))
  {
    fprintf(stderr, "Error at LUT malloc. \n");
    exit(EXIT_FAILURE);
  }
  for(uint32_t i=0; i<lut_entries5; i++) // 5-chars LUT
  {
    fmi->LUT[0][i] = malloc(2*sizeof(uint));
    fr =fread(fmi->LUT[0][i], sizeof(uint), 2, f);
    if(fr != 2)
    {
      fprintf(stderr, "Error at fread (LUT5)\n");
      exit(EXIT_FAILURE);
    }
  }
  for(uint32_t i=0; i<lut_entries6; i++) // 6-chars LUT
  {
    fmi->LUT[1][i] = malloc(2*sizeof(uint));
    fr = fread(fmi->LUT[1][i], sizeof(uint), 2, f);
    if(fr != 2)
    {
      fprintf(stderr, "Error at fread (LUT6)\n");
      exit(EXIT_FAILURE);
    }
  }
  fclose(f);
  return 0;
}

ssize_t
read_seq_from_fasta(FILE * fp, char** line)
{
  size_t len = 0;
  ssize_t read = 0;

  // read header
  read = getline(line, &len, fp);
  if (read == -1) return -1;
  if ((*line)[0] != '>') return -1;

  if ((read = getline(line, &len, fp)) == -1)
  {
     fprintf(stderr, "Error parsing FASTA sequence header\n");
     return -1;
  }

  if ( ((*line)[read-1] != '\n') && ((*line)[read-1] != '\r') )
  {
    (*line)[read] = 0;    // if (len == read) ????
  }
  else while ( ((*line)[read-1] == '\n') || ((*line)[read-1] == '\r') )
  {
    (*line)[read-1] = 0;
    read--;
  }

  return read;
}
