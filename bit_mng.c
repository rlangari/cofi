#include <assert.h>
#include <stdio.h>

#include "bit_mng.h"

/* write least significant bits */
void
write_char_to_buffer(uint8_t* buffer, uint n_bits, uint64_t position, uint8_t value)
{
  uint byte_pos = position / 8;
  uint8_t pos_in_byte = position % 8;
  uint8_t value_aux, mask;

  assert(n_bits <= 8);
  /* se cruza frontera de byte? */
  if (pos_in_byte + n_bits <= 8)
  {
      /* reset bits in the source byte */
      mask = ((1 << n_bits) - 1) << (8 - pos_in_byte - n_bits);
      buffer[byte_pos] = buffer[byte_pos] & (~mask);
      /* write new bits */
      mask = ((1 << n_bits) - 1);
      value = (value & mask) << (8 - pos_in_byte - n_bits);
      buffer[byte_pos] = buffer[byte_pos] | value;
  }
  else
  {
      /* en dos veces, primero hasta la frontera del byte (8 - pos_in_byte) */
      uint nbits_aux = 8 - pos_in_byte;
      /* reset bits in the source byte */
      mask = (1 << nbits_aux) - 1;
      buffer[byte_pos] = buffer[byte_pos] & (~mask);
      /* write new bits */
      value_aux = value >> (n_bits - nbits_aux);
      buffer[byte_pos] = buffer[byte_pos] | value_aux;
      //printf("mask: 0x%x - value_aux: 0x%x\n", mask, value_aux);

      /* y luego desde la frontera del byte (n_bits - 8 + pos_in_byte) */
      nbits_aux = n_bits - nbits_aux;
      /* reset bits in the source byte */
      mask = ((1 << nbits_aux) - 1) << (8 - nbits_aux);
      buffer[byte_pos+1] = buffer[byte_pos+1] & (~mask);
      /* write new bits */
      mask = ((1 << nbits_aux) - 1);
      value_aux = (value & mask) << (8 - nbits_aux);
      buffer[byte_pos+1] = buffer[byte_pos+1] | value_aux;
      //printf("mask: 0x%x - value_aux: 0x%x\n", mask, value_aux);
  }
}

uint8_t
read_char_from_buffer(uint8_t* buffer, uint n_bits, uint64_t position)
{
  uint64_t byte_pos = position/8;
  uint64_t pos_in_byte = position % 8;
  uint8_t value, value_aux, mask;

  assert(n_bits <= 8);

  /* se cruza frontera de byte? */
  if (pos_in_byte + n_bits <= 8)
  {
      mask = (1 << n_bits) - 1;
      value = buffer[byte_pos] >> (8 - pos_in_byte - n_bits);
      value = value & mask;
  }
  else
  {
      /* en dos veces, primero hasta la frontera del byte */
      uint nbits_aux = 8 - pos_in_byte;
      mask = (1 << nbits_aux) - 1;
      value = buffer[byte_pos] & mask;
      //printf("(%x-", value);

      /* y luego desde la frontera del byte */
      nbits_aux = n_bits - nbits_aux;
      mask = (1 << nbits_aux) - 1;
      value_aux = buffer[byte_pos+1] >> (8 - nbits_aux);
      value_aux = value_aux & mask;
      //printf("%x) ", value_aux);

      /* combinamos las dos secuencias de bits */
      value = (value << nbits_aux) | value_aux;
  }
  return value;
}
