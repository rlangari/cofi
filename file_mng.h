#ifndef _FILE_MNG_H_
#define _FILE_MNG_H_

#include <stdio.h>
#include "types.h"

/**
  @param file Char array containing the filename
  @param data Char array which will contain all the contents of the array.
  It will be alocated inside the function
  @return 0 if no error occurred.
*/
int file_to_char(const char * file, char ** data);

/**
  @param file Char array containing the filename
  @param fmi FMIndex which will be written to the file
  @return 0 if no error appeared.
*/
int write_fmindex(const char* file, struct FMIndex * fmi);

/**
  @param file Char array containing the filename
  @param fmi Loaded FMIndex
  @return 0 if no error appeared.
*/
int load_fmindex(const char* file, struct FMIndex * fmi);

/**
  @param fp Pointer to the file stream
  @param line String where the data will be saved.
  @return Size of the read sequence
*/
long int read_seq_from_fasta(FILE * fp, char** line);


#endif
