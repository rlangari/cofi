#ifndef _BWT_H_
#define _BWT_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <strings.h>
#include <time.h>
#include <sys/types.h>

#include "types.h"
#include "divsufsort.h"

/**
  * Fast ceiling of an unsigned integer division
  @result ceil(x/y)
*/
uint64_t
ceil_uint_div(uint64_t x, uint64_t y);

/**
  @param in Char array containing all the data
  @param out Char array containing the distinct elements in In. Allocated
  inside the function
  @result Number of unique elements. Negative if some error occurred.
*/
int get_unique_elements(char * in, char ** out, uint n);

/**
  @param in Char array containing the original text
  @param out Char array containing the BWT transform
  @param n Number of characters
  @param steps Number of steps proccessed in each iteration (see N-Steps FM-Index)
  @result Position of the final character of the string ($). Negative number if
  error.
*/
int get_bwt(char** in, char*** out, uint64_t n, uint steps, uint64_t** end);

/**
  @param bwt Char array containing the BWT. Will be modified inside.
  @param codes Char array containing the character substitution. The character
  at position i will be switched by the i number.
  @param n_bwt Number of characters in BWT
  @param n_characters Number of unique characters
  @result 0 if no error occurred
*/
int encode_bwt(char** bwt, char* codes, uint n_bwt, uint n_characters,
  uint steps, uint64_t* end);

/**
  @param bwt Char array containing the BWT.
  @param n Number of characters in BWT.
  @param n_bits Number of bits of each character.
  @param buffer Reduced bwt output. Allocated inside.
*/
int64_t reduce_bwt(char** bwt, uint64_t n, uint n_bits, uint8_t*** buffer, uint steps);

/**
  Function for sorting characters
*/
int char_cmp_func( const void *a, const void *b);
#endif
