#ifndef _PERF_H_
#define _PERF_H_

// numero de contadores hardware
#define HW_CTRS 4

// maximum number of executions
#define MAXRUNS  128

/* inicializa contadores hardware */
int perf_init();

/* resetea contadores hardware */
void perf_reset();

/* activa contadores hardware */
void perf_enable();

/* inicializa, resetea y activa contadores hardware */
int perf_start();

/* desactiva contadores hardware (dejan de contar los eventos) */
void perf_stop();

/* lee los contadores hw */
int perf_read_sample(uint32_t idx);

/* cierra los descriptores asociados a los contadores */
void perf_close();

/* imprime valor de contadores y cierra descriptores */
void perf_print();

/* imprime valor de muestras y media de contadores */
void perf_print_samples(int nruns);

#endif
