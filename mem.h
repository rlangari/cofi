#ifndef _MEM_H_
#define _MEM_H_

#include <stdio.h>
#include "types.h"

/* For explicit huge page allocation */
#define ADDR             (void *)(0x0UL)
#define PROTECTION       (PROT_READ | PROT_WRITE)

#define HUGE_PAGE_FLAGS  (MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB)
// #define MAP_HUGE_SHIFT   26
// #define MAP_HUGE_1GB     (30 << MAP_HUGE_SHIFT)
// #define HUGE_PAGE_FLAGS  (MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB | MAP_HUGE_1GB)

#define ALIGN_2MB 1U << 21

#define V_1MB    (1024.0*1024.0)
#define V_1GB    (V_1MB*1024.0)

// #define MEM_DDR4
// #define DUAL_PREFETCH
#define PREFETCH_HINT_1ST _MM_HINT_T1
#define PREFETCH_HINT_2ND _MM_HINT_T0
#define PREFETCH_HINT_L2 _MM_HINT_T1
#define PREFETCH_HINT_L1 _MM_HINT_T0

size_t get_hugepage_size();

void hugepages_status();

int mem_conf();

#endif
