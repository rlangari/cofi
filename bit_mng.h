#ifndef _BIT_MNG_H_
#define _BIT_MNG_H_

#include <stdint.h>

#include "types.h"

/**
  @param buffer Buffer to write the data
  @param n_bits Number of bits to write
  @param position Position to write inside the buffer (measured in bits)
  @param value Value to write
  @return 0 if no error occurred
*/
void write_char_to_buffer(uint8_t* buffer, uint n_bits, uint64_t position,
    uint8_t value);

/**
  @param buffer Buffer to read the data
  @param n_bits Number of bits to read
  @param position Position in the buffer to start reading (in  bits)
  @param return read char
*/
uint8_t read_char_from_buffer(uint8_t* buffer, uint n_bits, uint64_t position);


#endif
