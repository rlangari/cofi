#include <stdio.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>

#include "../file_mng.h"
//#include "../divsufsort.h"
#include "../BWT.h"
#include "../bit_mng.h"
#include "k15inc.h"

/* return wall time in seconds */
static double
get_wall_time()
{
		struct timeval time;
		if (gettimeofday(&time,NULL)) {
				exit(-1); // return 0;
		}
		return (double)time.tv_sec + (double)time.tv_usec * .000001;
}

int
main(int argc, const char *argv[])
{
	char * data;
	uint64_t data_len;
	char ** bwt;
	char * unique_data;
	char outfile[80];
	uint8_t ** reduced;
	SFM_t fmi;
	uint n_bits;
	uint64_t *end;
	int64_t reduced_len;
	uint64_t C[KSTEPS][SYMBOLS];
	double wall_0, wall_1;
	int n, unique_len;
	int verbose = 0;

	if (argc < 2)
	{
		fprintf(stderr, "Use: ./%s file\n", argv[0]);
		return 0;
	}
	if (argc == 3)
		verbose = 1;

	printf("Reading FM-index file %s... ", argv[1]);
	fflush(stdout);
	wall_0 = get_wall_time();
	n = file_to_char(argv[1], &data);
	if (n < 0) exit(1);
	data_len = strlen(data);
	wall_1 = get_wall_time();
	printf("OK\n");
	printf("Total time: %.3fs\n", wall_1 - wall_0);
	/*--------------------------------------------------------------------------*/

	// init_C(C);
	// build_C(&C[0][0], data, data_len);
	// printf("---C Array---(%lu elements)\n", data_len);
	// dump_C(C);
	/*--------------------------------------------------------------------------*/

	printf("Getting BWT of %lu characters... \n", data_len);
	fflush(stdout);
	wall_0 = get_wall_time();
	n = get_bwt(&data, &bwt, data_len, KSTEPS, &end);
	if (n < 0) exit(1);
	data_len++;  // $ character
	wall_1 = get_wall_time();
	printf("OK\nBWT Generated. Length: %lu\n", data_len);
	printf("Total time: %.3fs\n", wall_1 - wall_0);
	/*--------------------------------------------------------------------------*/

	// init_C(C);
	// for(int j=0; j<KSTEPS; j++)
	//  build_C(&C[j][0], &bwt[j][0], data_len);

	// printf("---C Array---(%lu elements)\n", data_len);
	// dump_C(C);
	/*--------------------------------------------------------------------------*/

	printf("Encoding BWT...\n");
	fflush(stdout);
	wall_0 = get_wall_time();
	unique_len = get_unique_elements(bwt[0], &unique_data, data_len);
	if (unique_len < 0) exit(1);

	n_bits = (uint)(ceil(log2(unique_len)));
	printf(" -> %d Unique elements. Each character can be stored in %u bits\n", unique_len, n_bits);
	// dump_array(unique_data, unique_len);
	qsort((void*)unique_data, unique_len, sizeof(char), char_cmp_func);
	// printf("unique_data: %s\n", unique_data);

	encode_bwt(bwt, unique_data, data_len, unique_len, KSTEPS, end);
	wall_1 = get_wall_time();
	printf("OK, encoded BWT\n");
	printf("Total time: %.3fs\n", wall_1 - wall_0);

	printf("Compressing BWT... ");
	fflush(stdout);
	wall_0 = get_wall_time();
	reduced_len = reduce_bwt(bwt, data_len, n_bits, &reduced, KSTEPS);
	if (reduced_len < 0) exit(1);
	wall_1 = get_wall_time();
	printf("OK\n -> Compression ratio: %.2f = %lu / %lu (original/compressed bytes)\n",
					(float) data_len / (reduced_len*KSTEPS), data_len, reduced_len*KSTEPS);
	printf("Total time: %.3fs\n", wall_1 - wall_0);
	free(bwt);
	/*--------------------------------------------------------------------------*/

	printf("Generating FM-index... ");
	fflush(stdout);
	wall_0 = get_wall_time();
	fmi.start = malloc(sizeof(char)*501);
	memcpy(fmi.start, data, 500);
	fmi.start[500] = 0;
	free(data);
	// dump_array(unique_data, unique_len);
	generate_SFM(&fmi, reduced, data_len, unique_data, 64, end);

	snprintf(outfile, sizeof(outfile), "%s.k%dinc.fmi", argv[1], KSTEPS);
	write_SFM(outfile, &fmi);
	wall_1 = get_wall_time();
	printf("OK -> FM-index written to file %s\n", outfile);
	printf("FM-index time: %.3fs\n", wall_1 - wall_0);
	printf("-------------------------------------------------\n\n");

	/*--------------------------------------------------------------------------*/

	exit(0);
}
