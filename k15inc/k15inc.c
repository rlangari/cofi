#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <ctype.h>
#include <sys/mman.h>
#ifdef KNL
#include <hbwmalloc.h>
#endif

#include "../aux.h"
#include "../mem.h"
#include "../file_mng.h"
#include "../BWT.h"
#include "../bit_mng.h"
#include "k15inc.h"

int
generate_SFM_encoding_table(SFM_t *fmi, uint8_t** out)
{
    *out = (uint8_t*) malloc(256*sizeof(uint8_t));

    for(uint i=0; i < 256; i++)
      (*out)[i] = -1;

    for(uint i=0; i < SYMBOLS; i++)
      (*out)[(uint8_t) fmi->alphabet[i]] = i;

    return 0;
}

int is_end(uint64_t index, uint64_t* end_char_pos) {
	for (int j = 0; j < KSTEPS; j++) {
		if (end_char_pos[j] == index) {
			return j + 1;
		}
	}
	return 0;
}

uint64_t read_char(uint64_t i, uint8_t** bwt, int end) {
	uint64_t c = 0;

	for (int j = KSTEPS-1; j >= end; j--) {
		c |= (uint64_t)read_char_from_buffer(bwt[j], BITS_PER_SYMBOL,
			i * BITS_PER_SYMBOL) << (BITS_PER_SYMBOL * j);
	}
	return c;
}

int
generate_SFM(SFM_t *fmi, uint8_t** bwt, uint64_t len,
						 char * alphabet, size_t alignment, uint64_t* end_char_pos)
{
	fmi->len = len;
	fmi->alphabet = alphabet;
	uint64_t offset_size = K2_SYMBOLS * sizeof(uint32_t) + sizeof(uint32_t);
	uint64_t changes_size = fmi->len * sizeof(uint32_t);
	// Auxiliary structure to keep index of the last inserted element
	uint32_t *offsets_aux;
	// Rocc memory allocation
	offsets_aux = aligned_alloc(alignment, offset_size);
	fmi->offsets = aligned_alloc(alignment, offset_size);
	if (fmi->offsets == NULL)
	{
		fprintf(stderr, "Error when malloc fm-index memory (Offsets)\n");
		return -1;
	}
	fmi->changes = aligned_alloc(alignment, changes_size);
	if (fmi->changes == NULL)
	{
		fprintf(stderr, "Error when malloc fm-index memory (Changes)\n");
		return -1;
	}

	for(uint64_t i=0; i <= K2_SYMBOLS; i++) {
		fmi->offsets[i] = 0;
		offsets_aux[i] = 0;
	}
	for(uint64_t i=0; i < fmi->len; i++) {
		fmi->changes[i] = UINT32_MAX;
	}

	uint64_t c;
	for(uint64_t i = 0; i < fmi->len; i++) {
		int end = is_end(i,end_char_pos);
		c = read_char(i,bwt,end);
		if (!end) c++;
		fmi->offsets[c]++;
	}

	for(uint64_t i=1; i <= K2_SYMBOLS; i++)
		fmi->offsets[i] += fmi->offsets[i-1];

	for(uint64_t i = 0; i < fmi->len; i++) {
		int end = is_end(i,end_char_pos);
		if (end) continue;
		c = read_char(i,bwt,end);

		fmi->changes[fmi->offsets[c]+offsets_aux[c]] = i;
		offsets_aux[c]++;
	}

	generate_SFM_encoding_table(fmi, &(fmi->encoding_table));

	return 0;
}

int write_SFM(const char* file, SFM_t *fmi)
{
	FILE *f = fopen(file, "w");
	if (f == NULL)
	{
		fprintf(stderr, "Cannot open file %s \n", file);
		return -1;
	}

	fwrite(fmi->start, sizeof(char), 500, f);
	fwrite(&fmi->len, sizeof(fmi->len), 1, f);
	fwrite(fmi->alphabet, sizeof(char), SYMBOLS, f);
	fwrite(fmi->encoding_table, sizeof(unsigned char), 256, f);
	fwrite(fmi->offsets, sizeof(uint32_t), K2_SYMBOLS + 1, f);
	fwrite(fmi->changes, sizeof(uint32_t), fmi->len, f);

	fclose(f);
	return 0;
}

int load_SFM(const char* file, SFM_t * fmi)
{
	FILE *f = fopen(file, "rb");
	if (f == NULL)
	{
		fprintf(stderr, "Cannot open file %s\n", file);
		return -1;
	}
	long fr;

	fmi->start = malloc(sizeof(char)*501);
	fr = fread(fmi->start, sizeof(char), 500, f);
	if (fr != 500)
	{
		fprintf(stderr, "Error at fread(start)\n");
		return -1;
	}
	fmi->start[500]=0;

	// Read prologue
	fr = fread(&fmi->len, sizeof(fmi->len), 1, f);
	if (fr != 1)
	{
		fprintf(stderr, "Error at fread(len)\n");
	}

	fmi->alphabet = malloc(SYMBOLS*sizeof(char));
	fr = fread(fmi->alphabet, sizeof(char), SYMBOLS, f);
	if (fr != SYMBOLS)
	{
		fprintf(stderr, "Error at fread(alphabet)\n");
		return -1;
	}

	fmi->encoding_table = (unsigned char*)malloc(sizeof(unsigned char)*256);
	fr =fread(fmi->encoding_table, sizeof(unsigned char), 256, f);
	if (fr != 256)
	{
		fprintf(stderr, "Error at fread (Encoding table)\n");
		exit(1);
	}

	uint64_t offset_size = K2_SYMBOLS * sizeof(uint32_t) + sizeof(uint32_t);
	uint64_t changes_size = fmi->len * sizeof(uint32_t);
#ifdef KNL
    // One malloc for two structures
	fr = hbw_posix_memalign_psize((void**)&fmi->offsets, 64,
			offset_size+changes_size, HBW_PAGESIZE_1GB);
	if(fr != 0)
	{
		fprintf(stderr, "%li Error at hbwmalloc (Offsets)\n", fr);
		exit(1);
	}
	fmi->changes = &fmi->offsets[K2_SYMBOLS+1];
#elif defined (HUGEPAGES)
  fmi->offsets = mmap(ADDR, offset_size+changes_size, PROTECTION, HUGE_PAGE_FLAGS, 0, 0);
  if (fmi->offsets == MAP_FAILED)
  {
    fflush(stdout);
    perror("  mmap");
    printf("  Failed to allocate offsets in huge pages.\n  Trying conventional memory ...");
    fr = posix_memalign((void**)&fmi->offsets, ALIGN_2MB, offset_size+changes_size);
    if (fr != 0)
    {
      fprintf(stderr, "%li Error at malloc (offsets)\n", fr);
      exit(1);
    }
    printf("OK\n");
  }
  else
  {
    printf("  offsets allocated in huge pages\n");
  }
  fmi->changes = &fmi->offsets[K2_SYMBOLS+1];
#else
	fr = posix_memalign((void**)&fmi->offsets,
			ALIGN_2MB, offset_size);
	if (fr != 0)
	{
		fprintf(stderr, "%li Error at malloc (Offsets)\n", fr);
		exit(1);
	}
	fr = posix_memalign((void**)&fmi->changes,
			ALIGN_2MB, changes_size);
	if (fr != 0)
	{
		fprintf(stderr, "%li Error at malloc (Changes)\n", fr);
		exit(1);
	}
#endif
	fr = fread(fmi->offsets, sizeof(uint32_t), K2_SYMBOLS + 1, f);
	if (fr != (long) K2_SYMBOLS + 1)
	{
		fprintf(stderr, "Error at fread (Offsets)\n");
		exit(1);
	}

	fr = fread(fmi->changes, sizeof(uint32_t), fmi->len, f);
	if (fr != (long) fmi->len)
	{
		fprintf(stderr, "Error at fread (Changes)\n");
		exit(1);
	}
	fclose(f);

	return 0;
}

void
free_SFM(SFM_t * fmi)
{
#ifdef KNL
	hbw_free(fmi->offsets);
	//hbw_free(fmi->changes);
#else
	free(fmi->offsets);
	free(fmi->changes);
#endif
	free(fmi->start);
	free(fmi->alphabet);
	free(fmi->encoding_table);
}

