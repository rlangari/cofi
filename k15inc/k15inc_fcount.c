#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <xmmintrin.h>
#include <float.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "../aux.h"
#include "../mem.h"
#include "../file_mng.h"
#include "../BWT.h"
#include "../bit_mng.h"
#include "../perf.h"
#include "k15inc.h"

#ifndef DEBUG_THREADS
    #define DEBUG_THREADS 0
#endif

#ifndef THREADS
    #ifdef KNL
        #define THREADS 256
    #else
        #define THREADS 20
    #endif
#endif

#ifndef NSEQS
    #define NSEQS     4
#endif

#define BYTES_PER_CACHE_BLOCK 64

#define DEFAULT_RUNS 2

/* disable IACA_START, IACA_END */
//#define IACA
#ifndef IACA
    #define IACA_START
    #define IACA_END
#else
    #include "../iacaMarks.h"
#endif

/* Use 1 nthreads and 1 nruns when making the histogram */
#define HIST 0

#if HIST
	#define HIST_SIZE 13
	uint64_t hist[HIST_SIZE];
	uint64_t hist_t[HIST_SIZE];
	uint64_t sizes[HIST_SIZE] = {0,1,4,16,64,256,1024,4096,16384,65536,262144,1048576,5000000000};
static __inline__ unsigned long long rdtsc() {
  unsigned hi, lo;
  __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
  return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}
#endif

static SFM_t fmi;

// Fastest way to convert chars A->0 C->1 G->2 T->3 (Not used)
static inline char decode_char(char c) {
	char bit_xor = c >> 2;
	char bits_ret = c >> 1;
	bits_ret ^= bit_xor;
	return bits_ret & 0b11;
}

// Returns next 15 letters
static inline uint64_t read_symbol(char *working_line, int *index) {
	uint64_t next_symbol = 0;
	for (uint32_t i = 0; i < KSTEPS; i++) {
		char let = fmi.encoding_table[*(working_line+*index-i)];
		next_symbol |= (uint64_t)let << (BITS_PER_SYMBOL*i);
	}
	*index -= KSTEPS;
	return next_symbol;
}

// Read firsts letters to make length multiple of K
static inline void read_start_symbol(uint32_t length, char *working_line, int *index, uint32_t *start_symbol, uint32_t *start_symbol_limit, uint64_t *lf) {
	uint32_t start_offset = length % KSTEPS;
	if (start_offset == 0) start_offset = KSTEPS;
	*index = length - 1;
	uint64_t next_symbol = 0;
	for (uint32_t i = 0; i < start_offset; i++) {
		char let = fmi.encoding_table[*(working_line+*index-i)];
		next_symbol |= (uint64_t)let << (BITS_PER_SYMBOL*(KSTEPS-start_offset+i));
	}
	*index -= start_offset;
	*lf += 2*start_offset;
	*start_symbol = next_symbol;
	*start_symbol_limit = next_symbol+(1LU << BITS_PER_SYMBOL*(KSTEPS-start_offset));
}

static inline void k15lf(uint64_t *left, uint64_t *right, uint32_t *elem) {
	char bs_completed = 0;
	char bs_seq_com[NSEQS];
	uint32_t mid, pivot;
	while (bs_completed < NSEQS) {
		for (uint j=0; j < NSEQS; j++) {
			if (bs_seq_com[j] == 0) {
				if (left[j] >= right[j]) {
					mid = left[j];
					pivot = elem[j];
				} else {
					mid = (right[j] + left[j]) / 2;
					pivot = fmi.changes[mid];
				}
				if (pivot < elem[j]) {
					left[j] = mid + 1;
				} else if (pivot > elem[j]) {
					right[j] = mid;
				} else {
					elem[j] = mid;
					bs_completed++;
					bs_seq_com[j] = 1;
				}
			}
		}
	}
}

static uint64_t __attribute__ ((noinline))
search(char **lines, uint *lines_len, uint count, uint *found, double *glfops)
{
  uint th_bl_size, total = 0;
  uint64_t lf = 0;
  double lfops = 0.0;

  omp_set_num_threads(THREADS);
  th_bl_size = count/THREADS;

  #pragma omp parallel reduction(+:total, lf, lfops) shared(fmi, lines)
  {
    uint32_t start[NSEQS], end[NSEQS];
    uint32_t start_next[NSEQS], end_next[NSEQS];
    int64_t left[NSEQS], right[NSEQS], limit[NSEQS];
    uint64_t next_symbol[NSEQS];
    int index[NSEQS];
    uint lengths[NSEQS], replaced_seqs = 0, finished_seqs = 0;
    char * working_lines[NSEQS];
    double start_time, end_time;

	uint32_t mid, pivot;
	int bs_completed = 0;
	int bs_seq_com[NSEQS] = {0};

    // Divide the sequences into blocks, one for each thread
    uint thread_id = omp_get_thread_num();
    uint bl_offset = thread_id*th_bl_size;
    uint block_len  = th_bl_size;
    if (thread_id < count % THREADS)
    {
      block_len++;
      bl_offset += thread_id;
    }
    else
      bl_offset += count % THREADS;

#if DEBUG_THREADS
    uint32_t cpu_num, node_num;
    int status;
    status = syscall(SYS_getcpu, &cpu_num, &node_num, NULL);
    if  (status != -1)
    {
        printf("  th.%2u -> core %2u -> node %u: %9u->%9u\n",
                thread_id, cpu_num, node_num, bl_offset, bl_offset + block_len - 1);
    }
#endif

    char ** lines_block = lines + bl_offset;
    uint *  llens_block = lines_len + bl_offset;

    start_time = omp_get_wtime();

    // Get starting positions for the sequences
    for(uint j=0; j < NSEQS; j++) {
		working_lines[j] = lines_block[j];
		lengths[j]       = llens_block[j];

		read_start_symbol(lengths[j], working_lines[j], &index[j], &start_next[j], &end_next[j], &lf);
		start[j] = fmi.offsets[start_next[j]];
		end[j] = fmi.offsets[end_next[j]];

		next_symbol[j] = read_symbol(working_lines[j], &index[j]);

		start_next[j] = UINT32_MAX;
    }

    while (finished_seqs < block_len) {
        /* start of loop to analyze */
        IACA_START

		// Read limits of the binary search and prefetch pivot
		for (uint j=0; j < NSEQS; j++) {
			left[j] = fmi.offsets[next_symbol[j]];
			right[j] = fmi.offsets[next_symbol[j]+1];
			limit[j] = fmi.offsets[next_symbol[j]+1];
			_mm_prefetch((char*) &fmi.changes[(right[j] + left[j]) / 2], PREFETCH_HINT_L2);
		}

#if HIST
		uint64_t column_size = right[0] - left[0];
		for (int i = 0; i < HIST_SIZE; i++) {
			if (column_size <= sizes[i]) {
				hist[i]++;
				break;
			}
		}
		uint64_t start_time = rdtsc();
#endif

		// Reset binary search counters
		for (uint j=0; j < NSEQS; j++) {
			bs_seq_com[j] = 0;
		}
		bs_completed = 0;

		// Binary search for start pointer
		while (bs_completed < NSEQS) {
			for (uint j=0; j < NSEQS; j++) {
				if (bs_seq_com[j] == 0) {
					mid = (left[j] >= right[j]) ? left[j] : (right[j] + left[j]) / 2;
					pivot = (left[j] >= right[j]) ? start[j] : fmi.changes[mid];
					bs_completed = (pivot == start[j]) ? bs_completed+1 : bs_completed;
					bs_seq_com[j] = (pivot == start[j]) ? 1 : 0;
					start[j] = (pivot == start[j]) ? mid : start[j];
					left[j] = (pivot < start[j]) ? mid+1 : left[j];
					right[j] = (pivot > start[j]) ? mid : right[j];

					/* Quick-sort using branch */
					//if (left[j] >= right[j]) {
					//	mid = left[j];
					//	pivot = start[j];
					//} else {
					//	mid = (right[j] + left[j]) / 2;
					//	pivot = fmi.changes[mid];
					//}
					//if (pivot < start[j]) {
					//	left[j] = mid + 1;
					//} else if (pivot > start[j]) {
					//	right[j] = mid;
					//} else {
					//	start[j] = mid;
					//	bs_completed++;
					//	bs_seq_com[j] = 1;
					//}
				}
			}
		}

		// Limit search boundaries
		for (uint j=0; j < NSEQS; j++) {
			left[j] = start[j];
			int64_t alt_right = left[j] + end[j] - fmi.changes[left[j]];
			limit[j] = limit[j] > alt_right ? alt_right : limit[j];
		}

		// Reset binary search counters
		for (uint j=0; j < NSEQS; j++) {
			bs_seq_com[j] = 0;
		}
		bs_completed = 0;

		// Binary search for end pointer
		while (bs_completed < NSEQS) {
			for (uint j=0; j < NSEQS; j++) {
				if (bs_seq_com[j] == 0) {
					mid = (left[j] >= limit[j]) ? left[j] : (limit[j] + left[j]) / 2;
					pivot = (left[j] >= limit[j]) ? end[j] : fmi.changes[mid];
					bs_completed = (pivot == end[j]) ? bs_completed+1 : bs_completed;
					bs_seq_com[j] = (pivot == end[j]) ? 1 : 0;
					end[j] = (pivot == end[j]) ? mid : end[j];
					left[j] = (pivot < end[j]) ? mid+1 : left[j];
					limit[j] = (pivot > end[j]) ? mid : limit[j];

					/* Quick-sort using branch */
					//if (left[j] >= limit[j]) {
					//	mid = left[j];
					//	pivot = end[j];
					//} else {
					//	mid = (limit[j] + left[j]) / 2;
					//	pivot = fmi.changes[mid];
					//}
					//if (pivot < end[j]) {
					//	left[j] = mid + 1;
					//} else if (pivot > end[j]) {
					//	limit[j] = mid;
					//} else {
					//	end[j] = mid;
					//	bs_completed++;
					//	bs_seq_com[j] = 1;
					//}
				}
			}
		}

#if HIST
		uint64_t final_time = rdtsc() - start_time;
		for (int i = 0; i < HIST_SIZE; i++) {
			if (column_size <= sizes[i]) {
				hist_t[i] += final_time;
				break;
			}
		}
#endif

		// Read next symbol and check if the sequence is finished
		for (uint j=0; j < NSEQS; j++) {
			next_symbol[j] = read_symbol(working_lines[j], &index[j]);
			_mm_prefetch((char*) &fmi.offsets[next_symbol[j]], PREFETCH_HINT_L2);
			_mm_prefetch((char*) &fmi.offsets[next_symbol[j]+1], PREFETCH_HINT_L2);
			if (index[j] < 0) {
				if (replaced_seqs + NSEQS >= block_len) {
					working_lines[j] = lines[count];
					lengths[j] = lines_len[count];
				} else {
					working_lines[j] = lines_block[replaced_seqs + NSEQS];
					lengths[j]       = llens_block[replaced_seqs + NSEQS];
				}
				replaced_seqs++;

				read_start_symbol(lengths[j], working_lines[j], &index[j], &start_next[j], &end_next[j], &lf);
				_mm_prefetch((char*) &fmi.offsets[start_next[j]], PREFETCH_HINT_L2);
				_mm_prefetch((char*) &fmi.offsets[end_next[j]], PREFETCH_HINT_L2);
			} else if (start_next[j] != UINT32_MAX) {
				finished_seqs++;
				total += end[j] - start[j];
				start[j] = fmi.offsets[start_next[j]];
				end[j] = fmi.offsets[end_next[j]];
				start_next[j] = UINT32_MAX;
			}
		}

		// update stats
		lf += 2*KSTEPS*NSEQS;
    }

    end_time = omp_get_wtime();
    lfops = lf/(end_time - start_time);

    /* end of loop to analyze */
    IACA_END

    #pragma omp barrier

  } //#pragma omp parallel

  (*found) = total;
  (*glfops) = lfops/GIGA;
  return lf;
}

static void
metrics(double *sample, uint64_t *lf, double *sample_glfops, int nruns)
{
    // Compute results
    double time_min = FLT_MAX, time_max = 0;
    double glfops_min = FLT_MAX, glfops_max = 0;
    double time_sum = 0, time_deviation_sum = 0, time_avg, time_deviation;
    uint32_t time_min_idx, time_max_idx;
    double glfops_sum = 0.0, glfops_avg;
    double GLF = lf[0]/GIGA;

    if (!elements_64b_equal(lf, nruns))
    {
        printf("ERROR: different runs performed different number of LFOPS\n");
        return;
    }

    for (int i = 1; i < nruns; i++)
    {
        time_sum += sample[i];
        if (sample[i] < time_min)
        {
            time_min = sample[i];
            time_min_idx = i;
        }
        if (sample[i] > time_max)
        {
            time_max = sample[i];
            time_max_idx = i;
        }

        glfops_sum += (sample_glfops[i]);
        if (sample_glfops[i] < glfops_min)
        {
            glfops_min = sample_glfops[i];
        }
        if (sample_glfops[i] > glfops_max)
        {
            glfops_max = sample_glfops[i];
        }
    }

    time_avg = time_sum / (nruns - 1);
    glfops_avg = glfops_sum / (nruns - 1);

    for (int i = 1; i < nruns; i++)
    {
        time_deviation_sum += (sample[i] - time_avg) * (sample[i] - time_avg);
    }
    time_deviation = sqrt(time_deviation_sum / (nruns-1));

    // Show metrics
    printf("Best throughput: %6.3f GLFOPS (%.3f)\n", GLF/time_min, glfops_max);
    printf("Avg. throughput: %6.3f GLFOPS (%.3f)\n", GLF/time_avg, glfops_avg);

    printf("Throughputs: [%.3f]", GLF/sample[0]);
    for (int i = 1; i < nruns; i++)
    {
        printf(" %.3f", GLF/sample[i]);
    }
    printf("\n");
    printf("Min time: %6.3f s (run #%u)\n", time_min, time_min_idx);
    printf("Avg time: %6.3f s\n", time_avg);
    printf("Max time: %6.3f s (run #%u)\n", time_max, time_max_idx);
    printf("Times: [%.3f]", sample[0]);
    for (int i = 1; i < nruns; i++)
    {
        printf(" %.3f", sample[i]);
    }
    printf("\n");
    printf("Standard deviation = %.3f%%\n", (time_deviation / time_avg) * 100);
}

////////////////////////////////////////////////////////////////////////////////
// Main function
////////////////////////////////////////////////////////////////////////////////

int
main(int argc, const char *argv[])
{
  uint total[MAXRUNS], count = 0, lines_size = 1000;
  int nruns = DEFAULT_RUNS;
  FILE * fp;
  char ** lines;
  uint * lines_len;
  ssize_t read;
  double start_timer, end_timer, sample[MAXRUNS];
  double start0, end0;
  double sample_glfops[MAXRUNS];
  uint64_t bases = 0, lf[MAXRUNS] = { 0 };

  printf("Program version: 20180126\n");
  printf(HLINE);

  if (argc < 3)
  {
    fprintf(stderr, "%i Use: ./%s file_fmi file_sequences [number_of_runs]\n", argc, argv[0]);
    return 0;
  }

  /*  The program runs the kernel nruns times (default 5) and
   *  reports the *best* result for any iteration after the first,
   *  therefore the minimum value for nruns is 2.
   *  The maximum allowable value for nruns is 128.
   *  Values larger than the default are unlikely to noticeably
   *  increase the reported performance.
   *  nruns can be set on the command line (third argument) */
  if (argc == 4)
  {
      nruns = atoi(argv[3]);
      if ((nruns < 1) || (nruns > MAXRUNS))
      {
          printf(HLINE);
          printf("** Unsupported number of runs: changed to %d (default) **\n", DEFAULT_RUNS);
          nruns = DEFAULT_RUNS;
      }
  }

#ifndef KNL
  // memory policy configuration
  mem_conf();
  printf(HLINE);
#endif

#if HIST
    for (int i = 0; i < HIST_SIZE; i++) {
        hist[i] = 0;
        hist_t[i] = 0;
    }
#endif

  printf("Loading FMIndex...\n");
  fflush(stdout);
  start_timer = omp_get_wtime();
  load_SFM(argv[1], &fmi);
  end_timer = omp_get_wtime();
  printf("OK. Index loaded in %fs\n", end_timer - start_timer);

  // free huge pages in each node
  hugepages_status();
  printf(HLINE);

  // Loading sequences into memory
  printf("Loading sequences...\n");
  fflush(stdout);
  start_timer = omp_get_wtime();
  lines = malloc(lines_size*sizeof(char*));
  lines_len = malloc(lines_size*sizeof(uint));
  if ( (lines == NULL) || (lines_len == NULL) )
  {
    printf("Error at malloc\n");
    exit(EXIT_FAILURE);
  }

  fp = fopen(argv[2], "r");
  if (fp == NULL)
  {
    printf("Error opening file %s\n", argv[2]);
    exit(EXIT_FAILURE);
  }

  while ((read = read_seq_from_fasta(fp, &(lines[count]))) >= 0)
  {
    lines_len[count] = read;
    bases += read;
    count++;
    if (count >= lines_size)
    {
      lines_size += 1000;
      lines = realloc(lines, lines_size*sizeof(char*));
      lines_len = realloc(lines_len, lines_size*sizeof(uint));
    }
  }
  fclose(fp);
  end_timer = omp_get_wtime();
  printf("OK. %.2f Msequences loaded in %fs (%.3f Mseq/s)\n",
          count/MEGA, end_timer - start_timer, (double)(count)/(MEGA*(end_timer - start_timer)));
  printf(HLINE);
  fflush(stdout);

  lines = realloc(lines, (count+1)*sizeof(char*));
  lines_len = realloc(lines_len, (count+1)*sizeof(uint));
  lines[count] = fmi.start;
  lines_len[count] = strlen(fmi.start);

  printf("Parameters\n");
  printf("- FM-index file: %s\n", argv[1]);
  printf("- Index size: %.1fGiB (%lu characters)\n", (double)(fmi.len)/GiB, fmi.len);
  printf("- Number of threads: %d\n", THREADS);
  printf("- Overlapped sequences: %d\n", NSEQS);
  printf("- Sequence file: %s\n", argv[2]);
  printf("- Number of bases: %.2f Gbases (%lu)\n", bases/GIGA, bases);
  printf("- Number of sequences: %.2f Mseq (%u)\n", count/MEGA, count);
  printf("- Average sequence length: %.2f bases\n", (double) bases/count);
  printf("- Number of sequences processed per thread: %.1fM (%u)\n", (double) (count)/(MEGA*THREADS), count/THREADS);
  printf(HLINE);
  printf("The kernel will be executed %d times.\n", nruns);
  printf("The *best* throughput will be reported (excluding the first iteration).\n");
  printf(HLINE);

  // Executing the FM-index count
  printf("Starting search... \n");
  fflush(stdout);

#ifdef PERF
  perf_init();
#endif

  start0 = omp_get_wtime();

  for (int run = 0; run < nruns; run++)
  {
#ifdef PERF
      perf_enable();
#endif

      start_timer = omp_get_wtime();
      lf[run] = search(lines, lines_len, count, &total[run], &sample_glfops[run]);
      end_timer = omp_get_wtime();
      sample[run] = end_timer - start_timer;

#ifdef PERF
      perf_stop();
      perf_read_sample(run);
      perf_reset();
#endif
  }

  end0 = omp_get_wtime();
  printf("OK\n");

  printf("Occurrences found:");
  if (elements_equal(total, nruns))
  {
      printf(" %u (in each run)\n", total[0]);
  }
  else
  {
      for (int i = 0; i < nruns; i++)
          printf(" %u", total[i]);
      printf("\n");
  }

  printf("Total LFOP: %.2fG (expected %.2fG)\n", sum(lf, nruns)/GIGA, 2*nruns*bases/GIGA);
  printf("Total time: %f\n", end0 - start0);
  printf("Raw throughput: %6.3f GLFOPS\n", sum(lf, nruns)/(end0 - start0)/GIGA);
  printf(HLINE);

  metrics(sample, lf, sample_glfops, nruns);
  printf(HLINE);

#ifdef PERF
  perf_print_samples(nruns);
  perf_close();
  printf(HLINE);
#endif

#if HIST
    printf(HLINE);
	printf("Accesses per columns:\n");
    for (int i = 0; i < HIST_SIZE; i++) {
        printf("%lu: %lu\n", sizes[i], hist[i]);
    }
    printf(HLINE);
	printf("Times per columns:\n");
    for (int i = 0; i < HIST_SIZE; i++) {
        printf("%lu: %lu\n", sizes[i], hist_t[i]);
    }
    printf(HLINE);
#endif

  fflush(stdout);

  if (lines)
  {
    for(uint i=0; i<count; i++) free(lines[i]);
    free(lines);
  }
  free_SFM(&fmi);
  return 0;
}

