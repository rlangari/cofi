#ifndef _SFM_H_
#define _SFM_H_

#include <immintrin.h>
#include <stdint.h>

typedef unsigned int uint;

#ifdef __GNUC__
#define __forceinline inline
//#define __forceinline __attribute__((always_inline))
#define _popcnt64 __builtin_popcountll
#endif

// k-steps
#define KSTEPS 15

// bits to encode a symbol (A,C,G,T)
#define BITS_PER_SYMBOL    2
// #define BITS_PER_K2_SYMBOL 4

// number of symbols
#define SYMBOLS     4
#define K2_SYMBOLS 1073741824 // 4^15

typedef struct LUT_entry {
	uint32_t start;
	uint32_t end;
} LUT_entry_t;

typedef struct SFM_Index {
	uint64_t len;     // BWT lenght
	char * start;
	char * alphabet;
	uint8_t * encoding_table;
	uint32_t * offsets;
	uint32_t * changes;
} SFM_t;

int generate_SFM_encoding_table(SFM_t *fmi, uint8_t** out);

int generate_SFM(SFM_t *fmi, uint8_t** bwt, uint64_t len, char* alphabet, size_t alignment, uint64_t* end_char);

int write_SFM(const char* file, SFM_t *fmi);

int load_SFM(const char* file, SFM_t * fmi);

void free_SFM(SFM_t * fmi);

#endif
